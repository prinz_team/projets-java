/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import com.sun.prism.paint.Color;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author ftonye
 */
import java.awt.GridLayout;
import javax.swing.JOptionPane;
public class BattleSea extends JFrame
{
    private static JButton[] listbtn;
    private static ArrayList<Ship> listShip;
    private static ArrayList<JButton> listJButtonInShip;
    
    
    private InetAddress adrs;
    
    private JButton btnStartServer;
    private JTextField txtPortNumber;
    private BackGroundCom com;
    private Thread t;
   
    
    private GridLayout grid;
    
    public BattleSea()
    {
        this.setSize(500,425);
        try {
            adrs = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(BattleSea.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        this.setTitle("BattleSea Server address is: " + adrs.getHostAddress());
        this.setLayout(new FlowLayout());
        
        createGrid();
        linkListenerToSeaSector();
               
        this.add(btnStartServer);
        this.add(txtPortNumber);
        
        CreateShips();
//    for (int i= 0; i < 5; i++){
//            CreateShips();
//    }
 
        
     
       
        
    }
    
    
    private void createGrid()
    {
        grid=new GridLayout(11, 10);
        this.setLayout(grid);
        this.setSize(1500, 1000);
        
        
        
        listbtn = new JButton[100];
        
        for(int i = 0; i< listbtn.length;i++)
        {
         listbtn[i] = new JButton(String.valueOf(i)); 
         listbtn[i].setSize(10,10);
         listbtn[i].setBackground(java.awt.Color.blue);          
         this.add(listbtn[i]);
        
        }
        
 
        
        JRadioButton Server = new JRadioButton("Server");
        JRadioButton Client = new JRadioButton("Client");
 
        ButtonGroup group = new ButtonGroup();
        group.add(Server);
        group.add(Client);
 
        add(Server);
        add(Client);
        
         if (Server.isSelected() == true) {
 
            JOptionPane.showMessageDialog(null, "Server"); 
 
        } else if (Client.isSelected() == true) {
 
 JOptionPane.showMessageDialog(null, "Server");
        }
        
        btnStartServer = new JButton("Start Server");
        btnStartServer.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae) {
               com = new BackGroundCom(Integer.parseInt(txtPortNumber.getText().toString()));
              t = new Thread(com);
               t.start();
            }
        
        });
        txtPortNumber = new JTextField();
        txtPortNumber.setText("Enter Port number");
//        btnStartServer.hide();
//        txtPortNumber.hide();
 
    }
    
    private void linkListenerToSeaSector()
    {
        for(int i = 0; i<listbtn.length;i++)
        {
             listbtn[i].addActionListener(new ActionListener()
          {
             @Override
             public void actionPerformed(ActionEvent ae) 
             {
                
                com.missileOutgoing = Integer.parseInt(ae.getActionCommand());
                com.dataToSend = true;
                 
                  
             }    
          });
        }
         
    }
    
   
       
    private void CreateShips()
    {
        int head ,nb = 0, disposition = 0;
        Random r = new Random();
        listShip = new ArrayList<Ship>();
        listJButtonInShip = new ArrayList<JButton>();
        
        
        for(int i = 0;i<5;i++)
        {
            try
            {
                ArrayList<JButton> boatsection = new ArrayList<JButton>();
                head = r.nextInt(98)+1;
                nb = r.nextInt(3)+1;
                disposition = r.nextInt(2)+1;
                for(int k = 7;k<100;k+=10)
                {  
                   if ((head ==k) || (head==k+1) || (head==k+2)) 
                   {
                       head-=3;
                   }

                   else {
                       head = head;
                   }
                }
                 
                 for(int cnt=0;cnt< nb ;cnt++)
                 {
                   if (disposition == 2) {
                    boatsection.add(listbtn[head+cnt]);
                   }
                   
                   else if (disposition == 1) {
                    boatsection.add(listbtn[head+cnt*10]);
                   }
//                    boatsection.add(listbtn[head+i*9]);
                   listJButtonInShip.add(listbtn[head+i]);
                 }
                 
                 Ship s = new Ship(boatsection);
                 listShip.add(s);
                 
            }
            catch(Exception ex)
            {
                
            }
//              JOptionPane.showMessageDialog(null, listShip);        
        }
          
    }
    
    public static void UpdateGrig(int incomming)
    {
        Boolean missHit = false;
        
        for(int i = 0; i<listbtn.length;i++)
        {
           
                
                  for(Ship element:listShip)
                  {
                      element.checkHit(listbtn[incomming]);
                     
                  }  
                 
                  
                
          
        }
        
        for(JButton element:listJButtonInShip)
        {
            if(!element.equals(listbtn[incomming]))
                missHit = true;
        }

         if(missHit)
         {
             listbtn[incomming].setBackground(java.awt.Color.magenta); 
         }
    }
}
