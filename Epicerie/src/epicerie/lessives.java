/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epicerie;

/**
 *
 * @author Phanuel
 */
class lessives extends produits {
    private String marque;
    public lessives()
    {
        super();
        setMarque ("MarquenonIdentifié");
    }

    public lessives (String nom, Double prix, String marque)
    {
        super(nom, prix);
        setMarque(marque);
    }

    public String getMarque()
    {
        return marque;
    }

    public void setMarque(String marque)
    {
        this.marque = marque;
    }
}
