/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epicerie;

/**
 *
 * @author Phanuel
 */
public interface Update {
    public void updateViande (String nom, double prix, String type);
    public void updateLegume (String nom, double prix, String type);
    public void updateLessive (String nom, double prix, String type);
}
