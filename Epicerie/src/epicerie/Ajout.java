/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epicerie;

/**
 *
 * @author Phanuel
 */
public interface Ajout {
    public void ajoutViande (String nom, double prix, String type);
    public void ajoutLegume (String nom, double prix, String type);
    public void ajoutLessive (String nom, double prix, String type);
}
