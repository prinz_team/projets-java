/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epicerie;

/**
 *
 * @author Phanuel
 */
class viandes extends produits {
    private String animal;
    private taxes taxe;
    public viandes()
    {
        super();
        setAnimal ("AnimalnonIdentifié");
    }

    public viandes (String nom, Double prix, String animal)
    {
        super(nom, prix);
        setAnimal(animal);
    }

    public String getAnimal()
    {
        return animal;
    }

    public void setAnimal(String animal)
    {
        this.animal = animal;
    }
}
