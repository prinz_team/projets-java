/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epicerie;

/**
 *
 * @author Phanuel
 */
public class produits {
    private String nom;
    private Double prix;
    public produits()
    {
        nom="Nondefini";
        prix=0.0;
    }

    public produits (String nom, Double prix)
    {
        setNom(nom);
        setPrix(prix);
    }

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public Double setPrix()
    {
        return prix;
    }

    public void setPrix(Double prix)
    {
        this.prix = prix;
    }
}
