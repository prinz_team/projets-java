/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epicerie;

/**
 *
 * @author Phanuel
 */
class legumes extends produits {
    private String type;
    public legumes()
    {
        super();
        setType ("Legume non Identifié");
    }

    public legumes (String nom, Double prix, String type)
    {
        super(nom, prix);
        setType(type);
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
}
