/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epicerie;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Phanuel
 */
public class AjoutBD implements Ajout {
    
    @Override
    public void ajoutViande (String nom, double prix, String type) {
        String url = "jdbc:mysql://localhost:3306/produits?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
        String user="root";
        String pass="";
            try {
                Connection con = DriverManager.getConnection(url,user,pass);
                String itemToInsert = "insert into viandes(nom, prix, type)" + "values(?,?,?);";
                PreparedStatement st = con.prepareStatement(itemToInsert);
                
                st.setString(1, nom);
                st.setDouble(2, prix);
                st.setString(3, type);

                st.executeUpdate();
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(Epicerie.class.getName()).log(Level.SEVERE, null, ex);
            }  
    }
    
     @Override
    public void ajoutLegume (String nom, double prix, String type) {
        String url = "jdbc:mysql://localhost:3306/produits?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
        String user="root";
        String pass="";
            try {
                Connection con = DriverManager.getConnection(url,user,pass);
                String itemToInsert = "insert into legumes(nom, prix, type)" + "values(?,?,?);";
                PreparedStatement st = con.prepareStatement(itemToInsert);
                
                st.setString(1, nom);
                st.setDouble(2, prix);
                st.setString(3, type);

                st.executeUpdate();
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(Epicerie.class.getName()).log(Level.SEVERE, null, ex);
            }  
    }
    
     @Override
    public void ajoutLessive (String nom, double prix, String type) {
        String url = "jdbc:mysql://localhost:3306/produits?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
        String user="root";
        String pass="";
            try {
                Connection con = DriverManager.getConnection(url,user,pass);
                String itemToInsert = "insert into lessives(nom, prix, type)" + "values(?,?,?);";
                PreparedStatement st = con.prepareStatement(itemToInsert);
                
                st.setString(1, nom);
                st.setDouble(2, prix);
                st.setString(3, type);

                st.executeUpdate();
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(Epicerie.class.getName()).log(Level.SEVERE, null, ex);
            }  
    }
}
