/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epicerie;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phanuel
 */
public class UpdateDB implements Update {
    @Override
    public void updateViande (String nom, double prix, String type) {
        String url = "jdbc:mysql://localhost:3306/produits?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
        String user="root";
        String pass="";
            try {
                Connection con = DriverManager.getConnection(url,user,pass);
                String updateItem = "update viandes set prix = ? where nom = ?;";
                PreparedStatement st = con.prepareStatement(updateItem);
                
                st.setString(2, nom);
                st.setDouble(1, prix);
                st.executeUpdate();
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(Epicerie.class.getName()).log(Level.SEVERE, null, ex);
            }  
    }
    
      @Override
    public void updateLegume (String nom, double prix, String type) {
        String url = "jdbc:mysql://localhost:3306/produits?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
        String user="root";
        String pass="";
            try {
                Connection con = DriverManager.getConnection(url,user,pass);
                String updateItem = "update legumes set prix = ? where nom = ?;";
                PreparedStatement st = con.prepareStatement(updateItem);
                
                st.setString(2, nom);
                st.setDouble(1, prix);
                st.executeUpdate();
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(Epicerie.class.getName()).log(Level.SEVERE, null, ex);
            }  
    }
    
      @Override
    public void updateLessive (String nom, double prix, String type) {
        String url = "jdbc:mysql://localhost:3306/produits?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
        String user="root";
        String pass="";
            try {
                Connection con = DriverManager.getConnection(url,user,pass);
                String updateItem = "update lessives set prix = ? where nom = ?;";
                PreparedStatement st = con.prepareStatement(updateItem);
                
                st.setString(2, nom);
                st.setDouble(1, prix);
                st.executeUpdate();
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(Epicerie.class.getName()).log(Level.SEVERE, null, ex);
            }  
    }
}
